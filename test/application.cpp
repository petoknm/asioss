#include "gtest/gtest.h"
#include "asioss/application.h"
#include <regex>

TEST(application, dispatchMatchesPath) {
    asioss::application app;
    int matched = 0;

    app.route(asioss::http_method::_get, "/test1", [&](auto &req, auto &res) {
        ASSERT_TRUE(true);
        matched++;
    });
    app.route(asioss::http_method::_get, "/test2", [&](auto &req, auto &res) {
        ASSERT_TRUE(false);
        matched++;
    });
    app.route(asioss::http_method::_get, "/test11", [&](auto &req, auto &res) {
        ASSERT_TRUE(false);
        matched++;
    });
    app.route(asioss::http_method::_get, "/test", [&](auto &req, auto &res) {
        ASSERT_TRUE(false);
        matched++;
    });

    app.dispatch(
            {
                    asioss::http_method::_get,
                    "/test1",
                    "body",
                    {},
                    {},
                    std::smatch{}
            },
            asioss::http_response(nullptr)
    );

    ASSERT_EQ(1, matched);
}

TEST(application, dispatchMatchesMethod) {
    asioss::application app;
    int matched = 0;

    app.route(asioss::http_method::_post, "/test1", [&](auto &req, auto &res) {
        ASSERT_TRUE(false);
        matched++;
    });
    app.route(asioss::http_method::_get, "/test1", [&](auto &req, auto &res) {
        ASSERT_TRUE(true);
        matched++;
    });

    app.dispatch(
            {
                    asioss::http_method::_get,
                    "/test1",
                    "body",
                    {},
                    {},
                    std::smatch{}
            },
            asioss::http_response(nullptr)
    );

    ASSERT_EQ(1, matched);
}

TEST(application, dispatchMatchesMultiple) {
    asioss::application app;
    int matched = 0;

    app.route(asioss::http_method::_get, "/test1", [&](auto &req, auto &res) {
        matched++;
    });
    app.route(asioss::http_method::_get, "/test11?", [&](auto &req, auto &res) {
        matched++;
    });

    app.dispatch(
            {
                    asioss::http_method::_get,
                    "/test1",
                    "body",
                    {},
                    {},
                    std::smatch{}
            },
            asioss::http_response(nullptr)
    );

    ASSERT_EQ(matched, 2);
}