# Asioss
Simple server based on Boost::Asio library.  
Under heavy development, currently starting to be multithreaded and asynchronous!

## Requirements
- [boost](http://www.boost.org/)
- [http-parser](https://github.com/nodejs/http-parser)

## ApacheBench results
much speed, such fast  
I'll try to keep the benchmarks updated as I go along. They represent a test server app that can be found in [main.cpp](https://github.com/petoknm/asioss/blob/master/src/main.cpp). I'm running on a pretty pimped out system (8 logical cores).  
Debug build
```shell
ab -c 10 -n 100000 "http://127.0.0.1:3333/fibonacci/5?id=456" 
This is ApacheBench, Version 2.3 <$Revision: 1757674 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 127.0.0.1 (be patient)
Completed 10000 requests
Completed 20000 requests
Completed 30000 requests
Completed 40000 requests
Completed 50000 requests
Completed 60000 requests
Completed 70000 requests
Completed 80000 requests
Completed 90000 requests
Completed 100000 requests
Finished 100000 requests


Server Software:        
Server Hostname:        127.0.0.1
Server Port:            3333

Document Path:          /fibonacci/5?id=456
Document Length:        0 bytes

Concurrency Level:      10
Time taken for tests:   2.777 seconds
Complete requests:      100000
Failed requests:        0
Total transferred:      1700000 bytes
HTML transferred:       0 bytes
Requests per second:    36015.40 [#/sec] (mean)
Time per request:       0.278 [ms] (mean)
Time per request:       0.028 [ms] (mean, across all concurrent requests)
Transfer rate:          597.91 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.0      0       1
Processing:     0    0   0.1      0       8
Waiting:        0    0   0.1      0       8
Total:          0    0   0.1      0       8

Percentage of the requests served within a certain time (ms)
  50%      0
  66%      0
  75%      0
  80%      0
  90%      0
  95%      0
  98%      0
  99%      0
 100%      8 (longest request)
```
Release build
```shell
ab -c 10 -n 100000 "http://127.0.0.1:3333/fibonacci/5?id=456" 
This is ApacheBench, Version 2.3 <$Revision: 1757674 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 127.0.0.1 (be patient)
Completed 10000 requests
Completed 20000 requests
Completed 30000 requests
Completed 40000 requests
Completed 50000 requests
Completed 60000 requests
Completed 70000 requests
Completed 80000 requests
Completed 90000 requests
Completed 100000 requests
Finished 100000 requests


Server Software:        
Server Hostname:        127.0.0.1
Server Port:            3333

Document Path:          /fibonacci/5?id=456
Document Length:        0 bytes

Concurrency Level:      10
Time taken for tests:   2.684 seconds
Complete requests:      100000
Failed requests:        0
Total transferred:      1700000 bytes
HTML transferred:       0 bytes
Requests per second:    37254.35 [#/sec] (mean)
Time per request:       0.268 [ms] (mean)
Time per request:       0.027 [ms] (mean, across all concurrent requests)
Transfer rate:          618.48 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.0      0       1
Processing:     0    0   0.0      0       3
Waiting:        0    0   0.0      0       3
Total:          0    0   0.1      0       3

Percentage of the requests served within a certain time (ms)
  50%      0
  66%      0
  75%      0
  80%      0
  90%      0
  95%      0
  98%      0
  99%      0
 100%      3 (longest request)
```