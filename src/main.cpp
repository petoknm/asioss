#include <iostream>
#include "asioss/application.h"

static int fib(int n) {
    if (n <= 1)
        return 1;
    return fib(n - 1) + fib(n - 2);
}

int main() {

    asioss::application app;

    app._get("/fibonacci/(\\d+)", [](asioss::http_request &req, asioss::http_response &res) {
        int n = std::stoi(req.url_matches[1]);
        std::ostream stream(res.rdbuf());
        stream << "Fibonacci(" << n << ") = " << fib(n) << std::endl;
    });

    app.run(8);

    return 0;
}
