#ifndef ASIOSS_APPLICATION_H
#define ASIOSS_APPLICATION_H

#include <regex>
#include "asioss/http_method.h"
#include "asioss/request_handler.h"
#include "asioss/http_request.h"
#include "asioss/tcp_server.h"

namespace asioss {

    class application {

        struct request_key {
            http_method method;
            std::string path;
            std::regex regex;
            request_handler handler;
        };

        std::vector<request_key> handlers;

        boost::asio::io_service io_service;
        tcp_server server;

    public:

        application();

        void route(http_method method, std::string path, request_handler handler);

        void _get(std::string path, request_handler handler);

        void _post(std::string path, request_handler handler);

        void _put(std::string path, request_handler handler);

        void _delete(std::string path, request_handler handler);

        // TODO: make dispatch() private
        void dispatch(http_request request, asioss::http_response response);

        void run(int num_threads);

    };
}

#endif //ASIOSS_APPLICATION_H
