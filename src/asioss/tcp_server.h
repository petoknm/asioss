#ifndef ASIOSS_TCP_SERVER_H
#define ASIOSS_TCP_SERVER_H

#include <boost/asio.hpp>

namespace asioss {
    class tcp_server {
        boost::asio::io_service &_io_service;
        boost::asio::ip::tcp::acceptor acceptor;

    public:
        tcp_server(boost::asio::io_service &io_service, int port = 3333)
                : _io_service(io_service),
                  acceptor(_io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(),
                                                                       static_cast<unsigned short>(port))) {}

        template<typename SocketStreamHandler>
        void run(SocketStreamHandler handler) {
            accept_connection(handler);
        }

    private:
        template<typename SocketStreamHandler>
        void accept_connection(SocketStreamHandler handler) {
            auto socket = std::make_shared<boost::asio::ip::tcp::iostream>();
            acceptor.async_accept(*socket->rdbuf(), [this, socket, handler](boost::system::error_code ec) {
                accept_connection(handler);
                handler(socket);
            });
        }
    };
}

#endif //ASIOSS_TCP_SERVER_H
