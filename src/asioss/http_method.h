#ifndef ASIOSS_HTTP_METHOD_H
#define ASIOSS_HTTP_METHOD_H

namespace asioss {
    enum class http_method {
        _get, _post, _put, _delete, _options
    };
}

#endif //ASIOSS_HTTP_METHOD_H
