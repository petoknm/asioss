#ifndef ASIOSS_HTTP_RESPONSE_H
#define ASIOSS_HTTP_RESPONSE_H

#include <streambuf>
#include <ostream>
#include <map>

namespace asioss {

    class http_response {
        int _status_code;
        std::map<std::string, std::string> headers;
        std::streambuf *_socket_streambuf;

    public:

        http_response(std::streambuf *socket_streambuf);

        int status_code() const;

        void status_code(int);

        void header(std::string field, std::string value);

        std::streambuf *rdbuf();

    private:

        void send();

    };
}

#endif //ASIOSS_HTTP_RESPONSE_H
