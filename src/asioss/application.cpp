#include <iostream>
#include "asioss/application.h"
#include "asioss/http_parser_wrapper.h"
#include <thread>

asioss::application::application()
        : handlers(),
          io_service(),
          server(io_service) {
}

void asioss::application::dispatch(asioss::http_request request, asioss::http_response response) {
    for (auto &h : handlers) {
        if (request.method == h.method && std::regex_match(request.path, request.url_matches, h.regex)) {
            h.handler(request, response);
        }
    }
}

void asioss::application::route(asioss::http_method method, std::string path, asioss::request_handler handler) {
    handlers.push_back({method, path, std::regex{path}, handler});
}

void asioss::application::_get(std::string path, asioss::request_handler handler) {
    route(asioss::http_method::_get, path, handler);
}

void asioss::application::_post(std::string path, asioss::request_handler handler) {
    route(asioss::http_method::_post, path, handler);
}

void asioss::application::_put(std::string path, asioss::request_handler handler) {
    route(asioss::http_method::_put, path, handler);
}

void asioss::application::_delete(std::string path, asioss::request_handler handler) {
    route(asioss::http_method::_delete, path, handler);
}

void asioss::application::run(int num_threads) {
    server.run([this](auto socket_stream) {
        http_parser_wrapper parser;
        parser.parse(*socket_stream, [this, socket_stream](http_request request) {
            dispatch(request, http_response(socket_stream->rdbuf()));
        });
    });
    std::vector<std::thread> threads;
    for(int i=0; i<num_threads; i++){
        threads.emplace_back([this](){
            io_service.run();
        });
    }
    for(auto& t : threads){
        t.join();
    }
}
