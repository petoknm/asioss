#ifndef ASIOSS_REQUEST_HANDLER_H
#define ASIOSS_REQUEST_HANDLER_H

#include <functional>
#include "asioss/http_request.h"
#include "asioss/http_response.h"

namespace asioss {
    typedef std::function<void(asioss::http_request &, asioss::http_response &)> request_handler;
}

#endif //ASIOSS_REQUEST_HANDLER_H
