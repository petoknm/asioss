#ifndef ASIOSS_HTTP_PARSER_WRAPPER_H
#define ASIOSS_HTTP_PARSER_WRAPPER_H

#include <http_parser.h>
#include <boost/asio/ip/tcp.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include "asioss/http_request.h"

namespace asioss {
    class http_parser_wrapper {

        http_parser parser;
        http_parser_settings settings;

    public:

        http_parser_wrapper() {
            http_parser_init(&parser, HTTP_REQUEST);
        }

        template<typename HttpRequestHandler>
        void parse(boost::asio::ip::tcp::iostream &socket_stream, HttpRequestHandler handler) {

            struct parser_data {
                HttpRequestHandler *handler;
                http_request request;
                std::vector<std::string> header_fields;
                std::vector<std::string> header_values;
                bool complete = false;
            };

            parser_data data;
            data.handler = &handler;
            parser.data = &data;

            // clear settings
            char *settings_ptr = reinterpret_cast<char *>(&settings);
            std::fill_n(settings_ptr, sizeof(settings), 0);

            settings.on_url = [](http_parser *p, const char *at, size_t length) {
                parser_data *d = reinterpret_cast<parser_data *>(p->data);
                auto &path = d->request.path = std::string(at, length);

                auto pos = path.find('?');
                if (pos != std::string::npos) {
                    // '?' was found at pos
                    auto without_query_params = path.substr(0, pos);
                    auto query_params = path.substr(pos + 1);
                    path = without_query_params;

                    using namespace boost::algorithm;
                    std::vector<std::string> query_param_parts;
                    split(query_param_parts, query_params, [](char v) { return v == '&'; }, token_compress_on);
                    for (auto &part : query_param_parts) {
                        std::vector<std::string> sub_parts;
                        split(sub_parts, part, [](char v) { return v == '='; }, token_compress_on);
                        d->request.query[sub_parts[0]] = sub_parts[1];
                    }
                }

                return 0;
            };

            settings.on_header_field = [](http_parser *p, const char *at, size_t length) {
                parser_data *d = reinterpret_cast<parser_data *>(p->data);
                if (d->header_fields.size() == d->header_values.size()) {
                    // start of a new header field name
                    d->header_fields.emplace_back();
                }
                d->header_fields.back() += std::string(at, length);
                return 0;
            };

            settings.on_header_value = [](http_parser *p, const char *at, size_t length) {
                parser_data *d = reinterpret_cast<parser_data *>(p->data);
                if (d->header_fields.size() != d->header_values.size()) {
                    // start of a new header value
                    d->header_values.emplace_back();
                }
                d->header_values.back() += std::string(at, length);
                return 0;
            };

            settings.on_headers_complete = [](http_parser *p) {
                parser_data *d = reinterpret_cast<parser_data *>(p->data);
                switch (p->method) {
                    case HTTP_GET:
                        d->request.method = http_method::_get;
                        break;
                    case HTTP_POST:
                        d->request.method = http_method::_post;
                        break;
                    case HTTP_PUT:
                        d->request.method = http_method::_put;
                        break;
                    case HTTP_DELETE:
                        d->request.method = http_method::_delete;
                        break;
                    case HTTP_OPTIONS:
                        d->request.method = http_method::_options;
                        break;
                }
                // convert arrays of header fields and values into a map
                while (!d->header_fields.empty()) {
                    d->request.headers[d->header_fields.back()] = d->header_values.back();
                    d->header_fields.pop_back();
                    d->header_values.pop_back();
                }
                return 0;
            };

            settings.on_body = [](http_parser *p, const char *at, size_t length) {
                parser_data *d = reinterpret_cast<parser_data *>(p->data);
                d->request.body = std::string(at, length);
                return 0;
            };

            settings.on_message_complete = [](http_parser *p) {
                parser_data *d = reinterpret_cast<parser_data *>(p->data);
                d->complete = true;
                (*d->handler)(d->request);
                return 0;
            };

            while (!data.complete) {
                std::string line;
                std::getline(socket_stream, line);
                line.append("\n");
                size_t nparsed = http_parser_execute(&parser, &settings, line.data(), line.size());
                if (parser.upgrade) {
                    /* handle new protocol */
                } else if (nparsed != line.size()) {
                    // TODO: error handling in http_parser_wrapper
                    data.complete = true;
                }
            }
        }

    };
}

#endif //ASIOSS_HTTP_PARSER_WRAPPER_H
