#ifndef ASIOSS_HTTP_REQUEST_H
#define ASIOSS_HTTP_REQUEST_H

#include <string>
#include <map>
#include <regex>
#include "asioss/http_method.h"

namespace asioss {
    struct http_request {
        http_method method;
        std::string path;
        std::string body;
        std::map<std::string, std::string> headers;
        std::map<std::string, std::string> query;

        std::smatch url_matches;
    };
}

#endif //ASIOSS_HTTP_REQUEST_H
