#include "http_response.h"

asioss::http_response::http_response(std::streambuf *socket_streambuf)
        : _status_code(200),
          _socket_streambuf(socket_streambuf) {
}

int asioss::http_response::status_code() const {
    return _status_code;
}

void asioss::http_response::status_code(int code) {
    _status_code = std::clamp(code, 100, 599);
//    _status_code = code;
}

std::streambuf *asioss::http_response::rdbuf() {
    send();
    return _socket_streambuf;
}

void asioss::http_response::header(std::string field, std::string value) {
    headers[field] = value;
}

void asioss::http_response::send() {
    std::ostream stream(_socket_streambuf);
    stream << "HTTP/1.1 " << _status_code << " OK\r\n";
    for (auto &p:headers) {
        stream << p.first << ": " << p.second << "\r\n";
    }
    stream << "Connection: close\r\n\r\n";
}
